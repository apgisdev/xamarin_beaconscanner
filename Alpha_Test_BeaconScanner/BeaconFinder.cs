﻿using System;

using Android.Content;
using Android.Util;
using EstimoteSdk.Common.Requirements;
using EstimoteSdk.Service;
using JavaObject = Java.Lang.Object;

namespace Alpha_Test_BeaconScanner
{
    
    /// <summary>
    /// Base class for managing the Estimote BeaconManager
    /// </summary>
    abstract class BeaconFinder : JavaObject, BeaconManager.IServiceReadyCallback
    {
        // A context provides a component hooks a component that has access to it to the rest of the application environment.
        // Put simply, context is the current state of the app/object. More reading needed for a comprehensive understanding.
        protected Context Context { get; private set; }
        protected BeaconManager BeaconManager { get; private set; }
        protected SystemRequirementsChecker Requirements { get; private set; }

        static readonly string TAG = typeof(BeaconFinder).Name;

        protected BeaconFinder(Context context)
        {
            this.Context = context;
            this.BeaconManager = new BeaconManager(context);
            //this.BeaconManager.Error += BeaconManager_Error;
        }

        /* - Not sure how necccesary this code is yet.
        void BeaconManager_Error(object sender, BeaconManager.ErrorEventArgs e)
        {
            Log.Error(TAG, "Something terrible happened with the BeaconManager, Error code {0}.", e.ErrorId);
        }
        */
        public abstract void OnServiceReady();

        public virtual void Stop()
        {
            BeaconManager.Disconnect();
        }
    }
}