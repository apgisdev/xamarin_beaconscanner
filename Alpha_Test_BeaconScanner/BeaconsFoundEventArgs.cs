﻿using System;
using System.Collections.Generic;
using System.Linq;

using EstimoteSdk;
using EstimoteSdk.Recognition.Packets;

namespace Alpha_Test_BeaconScanner
{
    class BeaconsFoundEventArgs : EventArgs
    {
        public Beacon[] Beacons { get; private set; }

        public BeaconsFoundEventArgs(IEnumerable<Beacon> beacons)
        {
            Beacons = beacons == null ? new Beacon[0] : beacons.ToArray();
        } 
    }
}