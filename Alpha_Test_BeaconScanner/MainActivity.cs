﻿using System;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;

using EstimoteSdk;
using EstimoteSdk.Recognition.Packets;

namespace Alpha_Test_BeaconScanner
{
    [Activity(Label = "Alpha_Test_BeaconScanner", MainLauncher = true)]
    public class MainActivity : Activity
    {
        Beacon _selectedBeacon;
        FindAllBeacons _findAllBeacons;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _findAllBeacons = new FindAllBeacons(this);
            _findAllBeacons.BeaconsFound += NewBeaconsFound;
            


        }

    }
}

