package mono.com.estimote.coresdk.connection.utils;


public class RecoveryHelper_RecoveryListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.estimote.coresdk.connection.utils.RecoveryHelper.RecoveryListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_recoveryFinished:()V:GetRecoveryFinishedHandler:EstimoteSdk.Connection.Utils.RecoveryHelper/IRecoveryListenerInvoker, Xamarin.Estimote.Android\n" +
			"";
		mono.android.Runtime.register ("EstimoteSdk.Connection.Utils.RecoveryHelper+IRecoveryListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", RecoveryHelper_RecoveryListenerImplementor.class, __md_methods);
	}


	public RecoveryHelper_RecoveryListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == RecoveryHelper_RecoveryListenerImplementor.class)
			mono.android.TypeManager.Activate ("EstimoteSdk.Connection.Utils.RecoveryHelper+IRecoveryListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void recoveryFinished ()
	{
		n_recoveryFinished ();
	}

	private native void n_recoveryFinished ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
