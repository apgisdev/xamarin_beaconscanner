package mono.com.estimote.coresdk.service;


public class BeaconManager_BeaconRangingListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.estimote.coresdk.service.BeaconManager.BeaconRangingListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onBeaconsDiscovered:(Lcom/estimote/coresdk/observation/region/beacon/BeaconRegion;Ljava/util/List;)V:GetOnBeaconsDiscovered_Lcom_estimote_coresdk_observation_region_beacon_BeaconRegion_Ljava_util_List_Handler:EstimoteSdk.Service.BeaconManager/IBeaconRangingListenerInvoker, Xamarin.Estimote.Android\n" +
			"";
		mono.android.Runtime.register ("EstimoteSdk.Service.BeaconManager+IBeaconRangingListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", BeaconManager_BeaconRangingListenerImplementor.class, __md_methods);
	}


	public BeaconManager_BeaconRangingListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == BeaconManager_BeaconRangingListenerImplementor.class)
			mono.android.TypeManager.Activate ("EstimoteSdk.Service.BeaconManager+IBeaconRangingListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onBeaconsDiscovered (com.estimote.coresdk.observation.region.beacon.BeaconRegion p0, java.util.List p1)
	{
		n_onBeaconsDiscovered (p0, p1);
	}

	private native void n_onBeaconsDiscovered (com.estimote.coresdk.observation.region.beacon.BeaconRegion p0, java.util.List p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
