package mono.com.estimote.coresdk.service;


public class BeaconManager_ConfigurableDevicesListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.estimote.coresdk.service.BeaconManager.ConfigurableDevicesListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onConfigurableDevicesFound:(Ljava/util/List;)V:GetOnConfigurableDevicesFound_Ljava_util_List_Handler:EstimoteSdk.Service.BeaconManager/IConfigurableDevicesListenerInvoker, Xamarin.Estimote.Android\n" +
			"";
		mono.android.Runtime.register ("EstimoteSdk.Service.BeaconManager+IConfigurableDevicesListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", BeaconManager_ConfigurableDevicesListenerImplementor.class, __md_methods);
	}


	public BeaconManager_ConfigurableDevicesListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == BeaconManager_ConfigurableDevicesListenerImplementor.class)
			mono.android.TypeManager.Activate ("EstimoteSdk.Service.BeaconManager+IConfigurableDevicesListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onConfigurableDevicesFound (java.util.List p0)
	{
		n_onConfigurableDevicesFound (p0);
	}

	private native void n_onConfigurableDevicesFound (java.util.List p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
