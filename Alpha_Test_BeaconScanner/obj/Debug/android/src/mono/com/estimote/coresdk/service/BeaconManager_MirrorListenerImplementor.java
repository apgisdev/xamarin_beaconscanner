package mono.com.estimote.coresdk.service;


public class BeaconManager_MirrorListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.estimote.coresdk.service.BeaconManager.MirrorListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onMirrorsFound:(Ljava/util/List;)V:GetOnMirrorsFound_Ljava_util_List_Handler:EstimoteSdk.Service.BeaconManager/IMirrorListenerInvoker, Xamarin.Estimote.Android\n" +
			"";
		mono.android.Runtime.register ("EstimoteSdk.Service.BeaconManager+IMirrorListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", BeaconManager_MirrorListenerImplementor.class, __md_methods);
	}


	public BeaconManager_MirrorListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == BeaconManager_MirrorListenerImplementor.class)
			mono.android.TypeManager.Activate ("EstimoteSdk.Service.BeaconManager+IMirrorListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onMirrorsFound (java.util.List p0)
	{
		n_onMirrorsFound (p0);
	}

	private native void n_onMirrorsFound (java.util.List p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
