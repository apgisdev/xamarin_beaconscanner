package mono.com.estimote.coresdk.service;


public class BeaconManager_MirrorMonitoringListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.estimote.coresdk.service.BeaconManager.MirrorMonitoringListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onEnteredRegion:(Lcom/estimote/coresdk/observation/region/mirror/MirrorRegion;Ljava/util/List;)V:GetOnEnteredRegion_Lcom_estimote_coresdk_observation_region_mirror_MirrorRegion_Ljava_util_List_Handler:EstimoteSdk.Service.BeaconManager/IMirrorMonitoringListenerInvoker, Xamarin.Estimote.Android\n" +
			"n_onExitedRegion:(Lcom/estimote/coresdk/observation/region/mirror/MirrorRegion;)V:GetOnExitedRegion_Lcom_estimote_coresdk_observation_region_mirror_MirrorRegion_Handler:EstimoteSdk.Service.BeaconManager/IMirrorMonitoringListenerInvoker, Xamarin.Estimote.Android\n" +
			"";
		mono.android.Runtime.register ("EstimoteSdk.Service.BeaconManager+IMirrorMonitoringListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", BeaconManager_MirrorMonitoringListenerImplementor.class, __md_methods);
	}


	public BeaconManager_MirrorMonitoringListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == BeaconManager_MirrorMonitoringListenerImplementor.class)
			mono.android.TypeManager.Activate ("EstimoteSdk.Service.BeaconManager+IMirrorMonitoringListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onEnteredRegion (com.estimote.coresdk.observation.region.mirror.MirrorRegion p0, java.util.List p1)
	{
		n_onEnteredRegion (p0, p1);
	}

	private native void n_onEnteredRegion (com.estimote.coresdk.observation.region.mirror.MirrorRegion p0, java.util.List p1);


	public void onExitedRegion (com.estimote.coresdk.observation.region.mirror.MirrorRegion p0)
	{
		n_onExitedRegion (p0);
	}

	private native void n_onExitedRegion (com.estimote.coresdk.observation.region.mirror.MirrorRegion p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
