package mono.com.estimote.coresdk.service;


public class BeaconManager_MirrorRangingListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.estimote.coresdk.service.BeaconManager.MirrorRangingListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onMirrorsDiscovered:(Lcom/estimote/coresdk/observation/region/mirror/MirrorRegion;Ljava/util/List;)V:GetOnMirrorsDiscovered_Lcom_estimote_coresdk_observation_region_mirror_MirrorRegion_Ljava_util_List_Handler:EstimoteSdk.Service.BeaconManager/IMirrorRangingListenerInvoker, Xamarin.Estimote.Android\n" +
			"";
		mono.android.Runtime.register ("EstimoteSdk.Service.BeaconManager+IMirrorRangingListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", BeaconManager_MirrorRangingListenerImplementor.class, __md_methods);
	}


	public BeaconManager_MirrorRangingListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == BeaconManager_MirrorRangingListenerImplementor.class)
			mono.android.TypeManager.Activate ("EstimoteSdk.Service.BeaconManager+IMirrorRangingListenerImplementor, Xamarin.Estimote.Android, Version=1.0.8.7, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onMirrorsDiscovered (com.estimote.coresdk.observation.region.mirror.MirrorRegion p0, java.util.List p1)
	{
		n_onMirrorsDiscovered (p0, p1);
	}

	private native void n_onMirrorsDiscovered (com.estimote.coresdk.observation.region.mirror.MirrorRegion p0, java.util.List p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
